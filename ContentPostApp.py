import webapp
from urllib.parse import parse_qs
import re

PAGE = """
<!DOCTYPE html>
<html lang='en'>
  <head>
    <script src='https://unpkg.com/htmx.org@1.9.10'></script>
  </head>
  <body>
    <div>
        <div>
            Contenido actual: <div id=content></div>
        </div>
        <button hx-get='/content/{resource}' hx-trigger='click' hx-target='#content' hx-swap='innerHTML'>
          Get
        </button>
    </div>
    <div>
        <div>
            Contenido nuevo: <input name='content' type='text'/>
        </div>
        <button hx-post='/content/{resource}' hx-include="[name='content']" hx-trigger='click' hx-target='#content' hx-swap='innerHTML'>
          Post
        </button>
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

class ContentApp(webapp.WebApp):

    def __init__(self, hostname, port):
        self.contents = {}
        super().__init__(hostname, port)

    def parse(self, request):
        headers, body = re.split(r'\r\n\r\n|\n\n', request, 1)
        parsed_body = parse_qs(body)
        method, resource = headers.split(' ', 2)[:2]
        return {'method': method, 'resource': resource, 'body': parsed_body}

    def process(self, data):
        method = data['method']
        if method == 'GET':
            return self.handle_request(data, self.get)
        elif method == 'POST':
            return self.handle_request(data, self.post)
        else:
            return "405 Method not allowed", PAGE_NOT_ALLOWED.format(method=data['method'])

    def handle_request(self, data, handler):
        try:
            return handler(data)
        except KeyError:
            return "404 Resource Not Found", PAGE_NOT_FOUND.format(resource=data['resource'])

    def get(self, data):
        resource = data['resource']
        if resource.startswith('/content/'):
            content_res = resource.split('/', 2)[2]
            page = self.contents.get(content_res, PAGE_NOT_FOUND.format(resource=resource))
            return "200 OK", page
        else:
            return "200 OK", PAGE.format(resource=resource[1:])

    def post(self, data):
        resource = data['resource']
        if resource.startswith('/content/'):
            content_res = resource.split('/', 2)[2]
            content = data['body'].get('content', [''])[0]
            self.contents[content_res] = content
            return "200 OK", content
        else:
            return "405 Method Not Allowed", PAGE_NOT_ALLOWED.format(method='POST')

if __name__ == "__main__":
    try:
        app = ContentApp("localhost", 8123)
    except KeyboardInterrupt:
        print("¡Hasta la próxima!")
